const fs = require('fs');
const _ = require('lodash');
const { Builder, By, Key, until, Select } = require('selenium-webdriver');

const po = JSON.parse(fs.readFileSync('./paymentOptions.json'));

const STATE_CODES = ['AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY'];

(async function example() {
  const payment1 = process.argv[2];
  const payment2 = process.argv[3];

  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('https://otc.cdc.nicusa.com/Public2.aspx?portal=montana&organization=City%20of%20Helena');

    const accountNo1 = _.split(_.get(po, 'address1.accountNo'), '-');

    await driver.wait(until.elementLocated(By.id('cart_itemId_0_field_value_0')), 10000);

    await driver.findElement(By.id('cart_itemId_0_field_value_0')).sendKeys(accountNo1[0]);
    await driver.findElement(By.id('cart_itemId_0_field_value_1')).sendKeys(accountNo1[1]);
    await driver.findElement(By.id('cart_itemId_0_field_value_2')).sendKeys(_.get(po, 'address1.phoneNo'));
    await driver.findElement(By.id('cart_itemId_0_field_value_3')).sendKeys(_.get(po, 'address1.serviceAddress'));
    await driver.findElement(By.id('cart_price_0')).sendKeys(payment1);

    if (_.get(po, 'address2')) {
      await driver.findElement(By.id('addAnotherCartItem_Button')).click()

      const accountNo2 = _.split(_.get(po, 'address2.accountNo'), '-');

      await driver.findElement(By.id('cart_itemId_1_field_value_0')).sendKeys(accountNo2[0]);
      await driver.findElement(By.id('cart_itemId_1_field_value_1')).sendKeys(accountNo2[1]);
      await driver.findElement(By.id('cart_itemId_1_field_value_2')).sendKeys(_.get(po, 'address2.phoneNo'));
      await driver.findElement(By.id('cart_itemId_1_field_value_3')).sendKeys(_.get(po, 'address2.serviceAddress'));
      await driver.findElement(By.id('cart_price_1')).sendKeys(payment2);
    }

    await driver.findElement(By.id('cartNextStep_Button')).click()

    await driver.wait(until.elementLocated(By.id('customer_firstName')), 10000);

    await driver.findElement(By.id('customer_firstName')).sendKeys(_.get(po, 'customerAddress.firstName'));
    await driver.findElement(By.id('customer_lastName')).sendKeys(_.get(po, 'customerAddress.lastName'));
    await driver.findElement(By.id('customer_address1')).sendKeys(_.get(po, 'customerAddress.address'));
    await driver.findElement(By.id('customer_zip')).sendKeys(_.get(po, 'customerAddress.zipcode'));
    await driver.findElement(By.id('customer_city')).sendKeys(_.get(po, 'customerAddress.city'));
    await driver.findElement(By.id('customer_phone')).sendKeys(_.get(po, 'customerAddress.phone'));
    await driver.findElement(By.id('customer_email')).sendKeys(_.get(po, 'customerAddress.email'));

    await driver.wait(() => {
      return driver.findElement(By.css('#s2id_customer_state > a > span')).getText().then(text => {
        let stateAbbreviationPresent = false;
        for (const stateCode of STATE_CODES) {
          if ((text + '').includes(stateCode)) {
            stateAbbreviationPresent = true;
            break;
          }
        }
        return stateAbbreviationPresent;
      })
    });

    await driver.wait(until.elementLocated(By.id('customerNextStep_Button')), 10000);
    await driver.findElement(By.id('customerNextStep_Button')).click();

    await driver.wait(until.elementLocated(By.id('creditCard_cardNumber')), 10000);

    await driver.findElement(By.id('creditCard_cardNumber')).sendKeys(_.get(po, 'paymentInfo.creditCardNumber'));
    await driver.findElement(By.id('creditCard_nameOnCard')).sendKeys(_.get(po, 'paymentInfo.name'));
    await driver.findElement(By.id('creditCard_securityCode')).sendKeys(_.get(po, 'paymentInfo.securityCode'));

    await driver.findElement(By.id("creditCard_expirationMonth")).click();
    {
      const dropdown = await driver.findElement(By.id("creditCard_expirationMonth"));
      await dropdown.findElement(By.xpath(`//option[@value='${_.get(po, 'paymentInfo.month')}']`)).click()
    }

    await driver.findElement(By.id("creditCard_expirationYear")).click();
    {
      const dropdown = await driver.findElement(By.id("creditCard_expirationYear"));
      await dropdown.findElement(By.xpath(`//option[@value='20${_.get(po, 'paymentInfo.year')}']`)).click()
    }
  } catch (e) {
    console.error(e);
  } finally { }
})();
