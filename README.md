Update `paymentOptions.json.sample` with your payment information. Remove the `.sample` suffix. To run the program simply run. 

```node main.js <payment1> <payment2>```

Such that `payment1` and `payment2` are numeric values that correspond to address one and address two in the json file. 

